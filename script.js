console.log("DOM Manipulation")

console.log(document.getElementById("demo"))
console.log(document.getElementsByTagName("h1"))
console.log(document.getElementsByClassName("title"))

console.log(document.querySelector("p"))

//change HTML elements
//property
//element.innerHTML = new html content

let test1 = document.querySelector(".title");
test1.innerHTML = `ArgleBargle`;

document.getElementById("demo").innerHTML = "Test Text";

document.querySelector(".title").style.color = "red"

//Event Listener
//element.addEventListener("event", cb())

let firstName = document.getElementById("txt-first-name");
let lastName = document.getElementById("txt-last-name");
let span = document.getElementById("span-full-name")

let updateName = () => {
    let firstTxt = firstName.value;
    let lastTxt = lastName.value;

    span.innerHTML = `${firstTxt} ${lastTxt}`
}

firstName.addEventListener("keyup", updateName);
lastName.addEventListener("keyup", updateName);

